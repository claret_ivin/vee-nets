﻿namespace VeeDMS
{
    partial class Home1
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelInvsubmenu = new System.Windows.Forms.Panel();
            this.InVN = new System.Windows.Forms.Button();
            this.InS = new System.Windows.Forms.Button();
            this.InVF = new System.Windows.Forms.Button();
            this.Invoice = new System.Windows.Forms.Button();
            this.Purchase = new System.Windows.Forms.Button();
            this.panelpurchasesubmenu = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.shipment = new System.Windows.Forms.Button();
            this.panelshipmentsubmenu = new System.Windows.Forms.Panel();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.linkLabel2 = new System.Windows.Forms.LinkLabel();
            this.linkLabel3 = new System.Windows.Forms.LinkLabel();
            this.panelInvsubmenu.SuspendLayout();
            this.panelpurchasesubmenu.SuspendLayout();
            this.panelshipmentsubmenu.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelInvsubmenu
            // 
            this.panelInvsubmenu.Controls.Add(this.InVN);
            this.panelInvsubmenu.Controls.Add(this.InS);
            this.panelInvsubmenu.Controls.Add(this.InVF);
            this.panelInvsubmenu.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelInvsubmenu.Location = new System.Drawing.Point(0, 51);
            this.panelInvsubmenu.Name = "panelInvsubmenu";
            this.panelInvsubmenu.Size = new System.Drawing.Size(835, 106);
            this.panelInvsubmenu.TabIndex = 8;
            // 
            // InVN
            // 
            this.InVN.BackColor = System.Drawing.Color.CadetBlue;
            this.InVN.Dock = System.Windows.Forms.DockStyle.Top;
            this.InVN.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.InVN.Location = new System.Drawing.Point(0, 70);
            this.InVN.Name = "InVN";
            this.InVN.Padding = new System.Windows.Forms.Padding(150, 0, 0, 0);
            this.InVN.Size = new System.Drawing.Size(835, 35);
            this.InVN.TabIndex = 2;
            this.InVN.Text = "Vee Nets";
            this.InVN.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.InVN.UseVisualStyleBackColor = false;
            this.InVN.Click += new System.EventHandler(this.button3_Click);
            // 
            // InS
            // 
            this.InS.BackColor = System.Drawing.Color.CadetBlue;
            this.InS.Dock = System.Windows.Forms.DockStyle.Top;
            this.InS.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.InS.Location = new System.Drawing.Point(0, 35);
            this.InS.Name = "InS";
            this.InS.Padding = new System.Windows.Forms.Padding(150, 0, 0, 0);
            this.InS.Size = new System.Drawing.Size(835, 35);
            this.InS.TabIndex = 1;
            this.InS.Text = "Seba Nets";
            this.InS.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.InS.UseVisualStyleBackColor = false;
            this.InS.Click += new System.EventHandler(this.button2_Click);
            // 
            // InVF
            // 
            this.InVF.BackColor = System.Drawing.Color.CadetBlue;
            this.InVF.Dock = System.Windows.Forms.DockStyle.Top;
            this.InVF.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.InVF.Location = new System.Drawing.Point(0, 0);
            this.InVF.Name = "InVF";
            this.InVF.Padding = new System.Windows.Forms.Padding(150, 0, 0, 0);
            this.InVF.Size = new System.Drawing.Size(835, 35);
            this.InVF.TabIndex = 0;
            this.InVF.Text = "VeeFish Nets";
            this.InVF.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.InVF.UseVisualStyleBackColor = false;
            this.InVF.Click += new System.EventHandler(this.button1_Click);
            // 
            // Invoice
            // 
            this.Invoice.BackColor = System.Drawing.Color.SteelBlue;
            this.Invoice.Dock = System.Windows.Forms.DockStyle.Top;
            this.Invoice.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Invoice.Font = new System.Drawing.Font("Times New Roman", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Invoice.ForeColor = System.Drawing.Color.Snow;
            this.Invoice.Location = new System.Drawing.Point(0, 0);
            this.Invoice.Name = "Invoice";
            this.Invoice.Size = new System.Drawing.Size(835, 51);
            this.Invoice.TabIndex = 7;
            this.Invoice.Text = "INVOICE";
            this.Invoice.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Invoice.UseVisualStyleBackColor = false;
            this.Invoice.Click += new System.EventHandler(this.Invoice_Click);
            // 
            // Purchase
            // 
            this.Purchase.BackColor = System.Drawing.Color.SteelBlue;
            this.Purchase.Dock = System.Windows.Forms.DockStyle.Top;
            this.Purchase.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Purchase.Font = new System.Drawing.Font("Times New Roman", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Purchase.ForeColor = System.Drawing.Color.Snow;
            this.Purchase.Location = new System.Drawing.Point(0, 157);
            this.Purchase.Name = "Purchase";
            this.Purchase.Size = new System.Drawing.Size(835, 51);
            this.Purchase.TabIndex = 9;
            this.Purchase.Text = "PURCHASE ORDERS";
            this.Purchase.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Purchase.UseVisualStyleBackColor = false;
            this.Purchase.Click += new System.EventHandler(this.button6_Click);
            // 
<<<<<<< HEAD

            // linkLabel3
            // 
            this.linkLabel3.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.linkLabel3.AutoSize = true;
            this.linkLabel3.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabel3.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline;
            this.linkLabel3.Location = new System.Drawing.Point(784, 627);
            this.linkLabel3.Name = "linkLabel3";
            this.linkLabel3.Size = new System.Drawing.Size(81, 23);
            this.linkLabel3.TabIndex = 13;
            this.linkLabel3.TabStop = true;
            this.linkLabel3.Text = "Vee Nets";
            // 
            // linkLabel2
            // 
            this.linkLabel2.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.linkLabel2.AutoSize = true;
            this.linkLabel2.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabel2.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline;
            this.linkLabel2.Location = new System.Drawing.Point(405, 627);
            this.linkLabel2.Name = "linkLabel2";
            this.linkLabel2.Size = new System.Drawing.Size(94, 23);
            this.linkLabel2.TabIndex = 12;
            this.linkLabel2.TabStop = true;
            this.linkLabel2.Text = "Seba Nets";
            // 
            // linkLabel1
            // 
            this.linkLabel1.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabel1.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline;
            this.linkLabel1.Location = new System.Drawing.Point(90, 627);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(124, 23);
            this.linkLabel1.TabIndex = 11;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "Vee Fish Nets";
            // 

=======
>>>>>>> 5aaa5131af2116f9e3c4966a298366f90148d73e
            // panelpurchasesubmenu
            // 
            this.panelpurchasesubmenu.Controls.Add(this.button1);
            this.panelpurchasesubmenu.Controls.Add(this.button2);
            this.panelpurchasesubmenu.Controls.Add(this.button3);
            this.panelpurchasesubmenu.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelpurchasesubmenu.Location = new System.Drawing.Point(0, 208);
            this.panelpurchasesubmenu.Name = "panelpurchasesubmenu";
            this.panelpurchasesubmenu.Size = new System.Drawing.Size(835, 106);
            this.panelpurchasesubmenu.TabIndex = 18;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.CadetBlue;
            this.button1.Dock = System.Windows.Forms.DockStyle.Top;
            this.button1.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(0, 70);
            this.button1.Name = "button1";
            this.button1.Padding = new System.Windows.Forms.Padding(150, 0, 0, 0);
            this.button1.Size = new System.Drawing.Size(835, 35);
            this.button1.TabIndex = 2;
            this.button1.Text = "Vee Nets";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.CadetBlue;
            this.button2.Dock = System.Windows.Forms.DockStyle.Top;
            this.button2.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(0, 35);
            this.button2.Name = "button2";
            this.button2.Padding = new System.Windows.Forms.Padding(150, 0, 0, 0);
            this.button2.Size = new System.Drawing.Size(835, 35);
            this.button2.TabIndex = 1;
            this.button2.Text = "Seba Nets";
            this.button2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.CadetBlue;
            this.button3.Dock = System.Windows.Forms.DockStyle.Top;
            this.button3.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Location = new System.Drawing.Point(0, 0);
            this.button3.Name = "button3";
            this.button3.Padding = new System.Windows.Forms.Padding(150, 0, 0, 0);
            this.button3.Size = new System.Drawing.Size(835, 35);
            this.button3.TabIndex = 0;
            this.button3.Text = "VeeFish Nets";
            this.button3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click_1);
            // 
            // shipment
            // 
            this.shipment.BackColor = System.Drawing.Color.SteelBlue;
            this.shipment.Dock = System.Windows.Forms.DockStyle.Top;
            this.shipment.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.shipment.Font = new System.Drawing.Font("Times New Roman", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.shipment.ForeColor = System.Drawing.Color.Snow;
            this.shipment.Location = new System.Drawing.Point(0, 314);
            this.shipment.Name = "shipment";
            this.shipment.Size = new System.Drawing.Size(835, 51);
            this.shipment.TabIndex = 19;
            this.shipment.Text = "SHIPMENT";
            this.shipment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.shipment.UseVisualStyleBackColor = false;
            this.shipment.Click += new System.EventHandler(this.shipment_Click);
            // 
            // panelshipmentsubmenu
            // 
            this.panelshipmentsubmenu.Controls.Add(this.button4);
            this.panelshipmentsubmenu.Controls.Add(this.button5);
            this.panelshipmentsubmenu.Controls.Add(this.button6);
            this.panelshipmentsubmenu.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelshipmentsubmenu.Location = new System.Drawing.Point(0, 365);
            this.panelshipmentsubmenu.Name = "panelshipmentsubmenu";
            this.panelshipmentsubmenu.Size = new System.Drawing.Size(835, 106);
            this.panelshipmentsubmenu.TabIndex = 20;
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.CadetBlue;
            this.button4.Dock = System.Windows.Forms.DockStyle.Top;
            this.button4.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.Location = new System.Drawing.Point(0, 70);
            this.button4.Name = "button4";
            this.button4.Padding = new System.Windows.Forms.Padding(150, 0, 0, 0);
            this.button4.Size = new System.Drawing.Size(835, 35);
            this.button4.TabIndex = 2;
            this.button4.Text = "Vee Nets";
            this.button4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.CadetBlue;
            this.button5.Dock = System.Windows.Forms.DockStyle.Top;
            this.button5.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.Location = new System.Drawing.Point(0, 35);
            this.button5.Name = "button5";
            this.button5.Padding = new System.Windows.Forms.Padding(150, 0, 0, 0);
            this.button5.Size = new System.Drawing.Size(835, 35);
            this.button5.TabIndex = 1;
            this.button5.Text = "Seba Nets";
            this.button5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.CadetBlue;
            this.button6.Dock = System.Windows.Forms.DockStyle.Top;
            this.button6.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.Location = new System.Drawing.Point(0, 0);
            this.button6.Name = "button6";
            this.button6.Padding = new System.Windows.Forms.Padding(150, 0, 0, 0);
            this.button6.Size = new System.Drawing.Size(835, 35);
            this.button6.TabIndex = 0;
            this.button6.Text = "VeeFish Nets";
            this.button6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.button6_Click_1);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.linkLabel3);
            this.panel1.Controls.Add(this.linkLabel2);
            this.panel1.Controls.Add(this.linkLabel1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 482);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(835, 100);
            this.panel1.TabIndex = 21;
            // 
            // linkLabel1
            // 
            this.linkLabel1.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.BackColor = System.Drawing.SystemColors.Control;
            this.linkLabel1.Location = new System.Drawing.Point(96, 47);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(72, 17);
            this.linkLabel1.TabIndex = 0;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "linkLabel1";
            // 
            // linkLabel2
            // 
            this.linkLabel2.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.linkLabel2.AutoSize = true;
            this.linkLabel2.BackColor = System.Drawing.SystemColors.Control;
            this.linkLabel2.Location = new System.Drawing.Point(381, 42);
            this.linkLabel2.Name = "linkLabel2";
            this.linkLabel2.Size = new System.Drawing.Size(72, 17);
            this.linkLabel2.TabIndex = 1;
            this.linkLabel2.TabStop = true;
            this.linkLabel2.Text = "linkLabel2";
            // 
            // linkLabel3
            // 
            this.linkLabel3.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.linkLabel3.AutoSize = true;
            this.linkLabel3.BackColor = System.Drawing.SystemColors.Control;
            this.linkLabel3.Location = new System.Drawing.Point(619, 47);
            this.linkLabel3.Name = "linkLabel3";
            this.linkLabel3.Size = new System.Drawing.Size(72, 17);
            this.linkLabel3.TabIndex = 2;
            this.linkLabel3.TabStop = true;
            this.linkLabel3.Text = "linkLabel3";
            // 
            // Home1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panelshipmentsubmenu);
            this.Controls.Add(this.shipment);
            this.Controls.Add(this.panelpurchasesubmenu);
            this.Controls.Add(this.Purchase);
            this.Controls.Add(this.panelInvsubmenu);
            this.Controls.Add(this.Invoice);
            this.Name = "Home1";
            this.Size = new System.Drawing.Size(835, 582);
            this.Load += new System.EventHandler(this.Home1_Load);
            this.panelInvsubmenu.ResumeLayout(false);
            this.panelpurchasesubmenu.ResumeLayout(false);
            this.panelshipmentsubmenu.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelInvsubmenu;
        private System.Windows.Forms.Button Invoice;
        private System.Windows.Forms.Button Purchase;
        private System.Windows.Forms.Button InVN;
        private System.Windows.Forms.Button InS;
        private System.Windows.Forms.Button InVF;
        private System.Windows.Forms.Panel panelpurchasesubmenu;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button shipment;
        private System.Windows.Forms.Panel panelshipmentsubmenu;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.LinkLabel linkLabel3;
        private System.Windows.Forms.LinkLabel linkLabel2;
        private System.Windows.Forms.LinkLabel linkLabel1;
    }
}
